#spec file for gv

Name: gv
Version: 3.7.4
Release: 20
Summary: gv is a Ghostscript PostScript interpreter by providing a graphical user interface
License: GPLv3+

URL: http://www.gnu.org/software/gv
Source0: https://ftp.gnu.org/gnu/gv/gv-%{version}.tar.gz
Source1: gv.png

Requires: ghostscript-x11
BuildRequires: /usr/bin/makeinfo
BuildRequires:  gcc Xaw3d-devel zlib-devel bzip2-devel desktop-file-utils
Requires(post): /sbin/install-info, /usr/bin/update-mime-database
Requires(post): /usr/bin/update-desktop-database
Requires(preun): /sbin/install-info
Requires(postun): /usr/bin/update-mime-database
Requires(postun): /usr/bin/update-desktop-database

Patch0:  gv-resource.patch
Patch4:  gv-bug1071238.patch

%description
GNU gv allows to view and navigate through PostScript and PDF documents on an
X display by providing a graphical user interface for the Ghostscript interpreter.

%package help
Summary: Documentation for gv

%description help
This package contains help documentation for gv


%prep
%setup -q
%patch0 -p1 -b .resource
%patch4 -p1 -b .bug1071238


%build
%configure
make %{?_smp_mflags}


%install
rm -rf $RPM_BUILD_ROOT
make install DESTDIR=$RPM_BUILD_ROOT

#provide link for gv with ghostview
ln $RPM_BUILD_ROOT%{_bindir}/gv $RPM_BUILD_ROOT%{_bindir}/ghostview

mkdir -p $RPM_BUILD_ROOT%{_datadir}/applications

cat > gv.desktop <<EOF
[Desktop Entry]
Name=GNU GV PostScript/PDF Viewer
GenericName=PostScript/PDF Viewer
Comment="View PostScript and PDF files"
Type=Application
Icon=gv
MimeType=application/postscript;application/pdf;
StartupWMClass=GV
Exec=gv %f
EOF

#putting application in the desktop menus
desktop-file-install --add-category=Applications --add-category=Graphics --dir %{buildroot}%{_datadir}/applications/ gv.desktop

#icon
mkdir -p $RPM_BUILD_ROOT%{_datadir}/pixmaps
cp -p %SOURCE1 $RPM_BUILD_ROOT%{_datadir}/pixmaps

#remove info dir file
rm -f ${RPM_BUILD_ROOT}%{_infodir}/dir


%post
/sbin/install-info %{_infodir}/%{name}.info %{_infodir}/dir || :
/usr/bin/update-mime-database /usr/share/mime > /dev/null 2>&1 || :
/usr/bin/update-desktop-database /usr/share/applications > /dev/null 2>&1 || :


%preun
if [[ $1 = 0 ]]; then
    /sbin/install-info --delete %{_infodir}/%{name}.info %{_infodir}/dir || :
fi


%postun
if [[ $1 = 0 ]]; then
    /usr/bin/update-mime-database /usr/share/mime > /dev/null 2>&1 || :
    /usr/bin/update-desktop-database /usr/share/applications > /dev/null 2>&1 || :
fi


%files
%doc AUTHORS ChangeLog COPYING NEWS README
%{_bindir}/ghostview
%{_bindir}/gv
%{_bindir}/gv-update-userconfig
%{_datadir}/gv/
%{_datadir}/applications/gv.desktop
%{_datadir}/info/gv.info.gz
%{_datadir}/pixmaps/gv.png

%files help
%{_mandir}/man1/gv.*
%{_mandir}/man1/gv-update-userconfig.*


%changelog
* Tue Dec 15 2020 zhanzhimin <zhanzhimin@huawei.com> - 3.7.4-20
- update source0

* Thu Jan 9 2020 openEuler Buildteam <buildteam@openeuler.org> - 3.7.4-19
- Type:enhancement
- ID:NA
- SUG:NA
- DESC: delete patches

* Thu Sep 26 2019 openEuler Buildteam <buildteam@openeuler.org> - 3.7.4-18
- Package init 
